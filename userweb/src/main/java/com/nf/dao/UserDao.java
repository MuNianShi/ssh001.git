package com.nf.dao;

import com.nf.entity.User;

public interface UserDao {
    //这是数据库层面的操作，
    //往往编写dao的人，不用考虑业务逻辑的
    //单对象例如，User,以及List,例如UserList
    //编写增删改查

    public User getUserById(String id);

    //public boolean delUserById();

    //public boolean updateUser();
}
