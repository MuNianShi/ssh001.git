package com.nf.action;

import com.nf.service.UserService;
import com.nf.service.impl.UserServiceImpl;
import com.opensymphony.xwork2.ActionSupport;

public class UserAction extends ActionSupport {

    //需要一个service，就定义service的接口为属性
    private UserService userService = null;
    public UserService getUserService() {
        return userService;
    }
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String execute() throws Exception {
        //userService=new UserServiceImpl();
        String userName = userService.getUserNameById("123");
        System.out.println(userName);
        return this.NONE;
    }
}
