package com.nf.service.impl;

import com.nf.dao.UserDao;
import com.nf.entity.User;
import com.nf.service.UserService;

public class UserServiceImpl implements UserService {
    private UserDao userDao = null;

    public UserDao getUserDao() {
        return userDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public String getUserNameById(String id) {
        User user = userDao.getUserById(id);
        if ("123456".equals(user.getUserPassword())){
            return user.getUserName()+"(弱密码)";
        }
        else {
            return user.getUserName();
        }

    }
}
